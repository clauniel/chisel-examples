module Hello(input clk, input reset,
    output io_led
);

  reg[0:0] blk;
  wire T0;
  wire[24:0] T1;
  reg[24:0] r1;
  wire[24:0] T2;
  wire[24:0] T3;
  wire[24:0] T4;
  wire[24:0] T5;
  wire T6;

  assign io_led = blk;
  assign T0 = r1 == T1;
  assign T1 = {2'h0/* 0*/, 23'h7a11ff/* 7999999*/};
  assign T2 = T0 ? T5 : T3;
  assign T3 = r1 + T4;
  assign T4 = {24'h0/* 0*/, 1'h1/* 1*/};
  assign T5 = {24'h0/* 0*/, 1'h0/* 0*/};
  assign T6 = ~ blk;

  always @(posedge clk) begin
    if(reset) begin
      blk <= 1'h0/* 0*/;
    end else if(T0) begin
      blk <= T6;
    end
    r1 <= reset ? 25'h0/* 0*/ : T2;
  end
endmodule

