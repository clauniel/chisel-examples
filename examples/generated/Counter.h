#ifndef __Counter__
#define __Counter__

#include "emulator.h"

class Counter_t : public mod_t {
 public:
  dat_t<1> Counter__io_button;
  dat_t<1> Counter__io_button__prev;
  dat_t<25> Counter__delayReg;
  dat_t<25> Counter__delayReg_shadow;
  dat_t<25> Counter__delayReg__prev;
  dat_t<8> Counter__counterReg;
  dat_t<8> Counter__counterReg_shadow;
  dat_t<8> Counter__counterReg__prev;
  dat_t<8> Counter__io_leds;
  dat_t<8> Counter__io_leds__prev;
  int clk;
  int clk_cnt;

  void init ( bool rand_init = false );
  void clock_lo ( dat_t<1> reset );
  void clock_hi ( dat_t<1> reset );
  int clock ( dat_t<1> reset );
  void print ( FILE* f );
  bool scan ( FILE* f );
  void dump ( FILE* f, int t );
};

#endif
