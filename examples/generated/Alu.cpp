#include "Alu.h"

void Alu_t::init ( bool rand_init ) {
}
void Alu_t::clock_lo ( dat_t<1> reset ) {
  val_t Alu__b__w0;
  { Alu__b__w0 = Alu__io_sw.values[0] >> 6; }
  Alu__b__w0 = Alu__b__w0 & 15;
  val_t Alu__a__w0;
  { Alu__a__w0 = Alu__io_sw.values[0] >> 2; }
  Alu__a__w0 = Alu__a__w0 & 15;
  val_t T0__w0;
  { T0__w0 = Alu__a__w0+Alu__b__w0; }
  T0__w0 = T0__w0 & 15;
  val_t Alu__fn__w0;
  { Alu__fn__w0 = Alu__io_sw.values[0]; }
  Alu__fn__w0 = Alu__fn__w0 & 3;
  val_t T1__w0;
  T1__w0 = Alu__fn__w0 == 0x0L;
  val_t T2__w0;
  { val_t __mask = -T1__w0; T2__w0 = 0x0L ^ ((0x0L ^ T0__w0) & __mask); }
  val_t T3__w0;
  { T3__w0 = Alu__a__w0-Alu__b__w0; }
  T3__w0 = T3__w0 & 15;
  val_t T4__w0;
  T4__w0 = Alu__fn__w0 == 0x1L;
  val_t T5__w0;
  { val_t __mask = -T4__w0; T5__w0 = T2__w0 ^ ((T2__w0 ^ T3__w0) & __mask); }
  val_t T6__w0;
  { T6__w0 = Alu__a__w0|Alu__b__w0; }
  val_t T7__w0;
  T7__w0 = Alu__fn__w0 == 0x2L;
  val_t T8__w0;
  { val_t __mask = -T7__w0; T8__w0 = T5__w0 ^ ((T5__w0 ^ T6__w0) & __mask); }
  val_t T9__w0;
  { T9__w0 = Alu__a__w0&Alu__b__w0; }
  val_t T10__w0;
  T10__w0 = Alu__fn__w0 == 0x3L;
  val_t T11__w0;
  { val_t __mask = -T10__w0; T11__w0 = T8__w0 ^ ((T8__w0 ^ T9__w0) & __mask); }
  val_t T12__w0;
  { T12__w0 = T11__w0 | 0x0L << 4; }
  { Alu__io_led.values[0] = T12__w0; }
}
void Alu_t::clock_hi ( dat_t<1> reset ) {
}
int Alu_t::clock ( dat_t<1> reset ) {
  uint32_t min = ((uint32_t)1<<31)-1;
  if (clk_cnt < min) min = clk_cnt;
  clk_cnt-=min;
  if (clk_cnt == 0) clock_lo( reset );
  if (clk_cnt == 0) clock_hi( reset );
  if (clk_cnt == 0) clk_cnt = clk;
  return min;
}
void Alu_t::print ( FILE* f ) {
  fprintf(f, "%s", TO_CSTR(Alu__io_led));
  fprintf(f, "\n");
  fflush(f);
}
bool Alu_t::scan ( FILE* f ) {
  str_to_dat(read_tok(f), Alu__io_sw);
  return(!feof(f));
}
void Alu_t::dump(FILE *f, int t) {
}
