#include "Counter.h"

void Counter_t::init ( bool rand_init ) {
  if (rand_init) Counter__delayReg.randomize();
  if (rand_init) Counter__counterReg.randomize();
}
void Counter_t::clock_lo ( dat_t<1> reset ) {
  val_t T0__w0;
  { val_t __mask = -reset.values[0]; T0__w0 = Counter__delayReg.values[0] ^ ((Counter__delayReg.values[0] ^ 0x0L) & __mask); }
  val_t T1__w0;
  { T1__w0 = Counter__delayReg.values[0]+0x1L; }
  T1__w0 = T1__w0 & 33554431;
  val_t T2__w0;
  T2__w0 = Counter__io_button.values[0] == 0x0L;
  val_t T3__w0;
  T3__w0 = !reset.values[0];
  val_t T4__w0;
  { T4__w0 = T3__w0&&T2__w0; }
  val_t T5__w0;
  { val_t __mask = -T4__w0; T5__w0 = T0__w0 ^ ((T0__w0 ^ T1__w0) & __mask); }
  val_t T6__w0;
  T6__w0 = Counter__delayReg.values[0] == 0x3L;
  val_t T7__w0;
  { val_t __mask = -T6__w0; T7__w0 = T5__w0 ^ ((T5__w0 ^ 0x0L) & __mask); }
  { Counter__delayReg_shadow.values[0] = TERNARY(reset.values[0], 0x0L, T7__w0); }
  val_t T8__w0;
  { val_t __mask = -reset.values[0]; T8__w0 = Counter__counterReg.values[0] ^ ((Counter__counterReg.values[0] ^ 0x0L) & __mask); }
  val_t T9__w0;
  { T9__w0 = reset.values[0]||T2__w0; }
  val_t T10__w0;
  T10__w0 = !T9__w0;
  val_t T11__w0;
  { val_t __mask = -T10__w0; T11__w0 = T8__w0 ^ ((T8__w0 ^ Counter__counterReg.values[0]) & __mask); }
  val_t T12__w0;
  { T12__w0 = Counter__counterReg.values[0]+0x1L; }
  T12__w0 = T12__w0 & 255;
  val_t T13__w0;
  { val_t __mask = -T6__w0; T13__w0 = T11__w0 ^ ((T11__w0 ^ T12__w0) & __mask); }
  { Counter__counterReg_shadow.values[0] = TERNARY(reset.values[0], 0x0L, T13__w0); }
  { Counter__io_leds.values[0] = Counter__counterReg.values[0]; }
}
void Counter_t::clock_hi ( dat_t<1> reset ) {
  Counter__delayReg = Counter__delayReg_shadow;
  Counter__counterReg = Counter__counterReg_shadow;
}
int Counter_t::clock ( dat_t<1> reset ) {
  uint32_t min = ((uint32_t)1<<31)-1;
  if (clk_cnt < min) min = clk_cnt;
  clk_cnt-=min;
  if (clk_cnt == 0) clock_lo( reset );
  if (clk_cnt == 0) clock_hi( reset );
  if (clk_cnt == 0) clk_cnt = clk;
  return min;
}
void Counter_t::print ( FILE* f ) {
  fprintf(f, "%s", TO_CSTR(Counter__io_leds));
  fprintf(f, "\n");
  fflush(f);
}
bool Counter_t::scan ( FILE* f ) {
  str_to_dat(read_tok(f), Counter__io_button);
  return(!feof(f));
}
void Counter_t::dump(FILE *f, int t) {
  if (t == 0) {
    fprintf(f, "$timescale 1ps $end\n");
    fprintf(f, "$scope module Counter $end\n");
    fprintf(f, "$var wire 1 N0 reset $end\n");
    fprintf(f, "$var wire 1 N1 io_button $end\n");
    fprintf(f, "$var wire 25 N2 delayReg $end\n");
    fprintf(f, "$var wire 8 N3 counterReg $end\n");
    fprintf(f, "$var wire 8 N4 io_leds $end\n");
    fprintf(f, "$upscope $end\n");
    fprintf(f, "$enddefinitions $end\n");
    fprintf(f, "$dumpvars\n");
    fprintf(f, "$end\n");
  }
  fprintf(f, "#%d\n", t);
  if (t == 0 || (Counter__io_button != Counter__io_button__prev).to_bool())
    dat_dump(f, Counter__io_button, "N1");
  Counter__io_button__prev = Counter__io_button;
  if (t == 0 || (Counter__delayReg != Counter__delayReg__prev).to_bool())
    dat_dump(f, Counter__delayReg, "N2");
  Counter__delayReg__prev = Counter__delayReg;
  if (t == 0 || (Counter__counterReg != Counter__counterReg__prev).to_bool())
    dat_dump(f, Counter__counterReg, "N3");
  Counter__counterReg__prev = Counter__counterReg;
  if (t == 0 || (Counter__io_leds != Counter__io_leds__prev).to_bool())
    dat_dump(f, Counter__io_leds, "N4");
  Counter__io_leds__prev = Counter__io_leds;
}
