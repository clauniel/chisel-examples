import Chisel._
import Node._
import scala.collection.mutable.HashMap

class Mux2 extends Module {
	val io = new Bundle {
		val select = UInt( INPUT, 1 )
		val a = UInt( INPUT, 1 )
		val b = UInt( INPUT, 1 )
		val out = UInt( OUTPUT, 1 )
	}
	when ( io.select === UInt( 0 ) ) {
		io.out := io.a
	} .elsewhen ( io.select === UInt( 1 ) ) {
		io.out := io.b 
	} .otherwise {
		io.out := UInt( 0 )
	}

}

object Mux2Main {
	def main( args: Array[String] ): Unit = {
		chiselMain( args, () => Module( new Mux2()))
	}
}


class Mux2Tester( dut: Mux2 ) extends Tester( dut, Array( dut.io ) ) {
	defTests {
		var allGood = true
		val vars = new HashMap[ Node, Node ]()

		vars( dut.io.a ) = UInt( 0, 1 )
		vars( dut.io.b ) = UInt( 1, 1 )
		vars( dut.io.select ) = UInt( 0, 1 )
		vars( dut.io.out ) = UInt( 0, 1 )
		allGood = step(vars) && allGood
		vars( dut.io.select ) = UInt( 1, 1 )
		vars( dut.io.out ) = UInt( 1, 1 )
		allGood = step(vars) && allGood 

		allGood
	}
}

object Mux2Tester {
	def main( args: Array[String] ): Unit = {
		chiselMainTest( args, () => Module( new Mux2 ) ) {
			f => new Mux2Tester( f )
		}
	}
}
