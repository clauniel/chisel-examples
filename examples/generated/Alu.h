#ifndef __Alu__
#define __Alu__

#include "emulator.h"

class Alu_t : public mod_t {
 public:
  dat_t<10> Alu__io_sw;
  dat_t<10> Alu__io_led;
  int clk;
  int clk_cnt;

  void init ( bool rand_init = false );
  void clock_lo ( dat_t<1> reset );
  void clock_hi ( dat_t<1> reset );
  int clock ( dat_t<1> reset );
  void print ( FILE* f );
  bool scan ( FILE* f );
  void dump ( FILE* f, int t );
};

#endif
