module Mux2(
    input  io_select,
    input  io_a,
    input  io_b,
    output io_out
);

  wire T0;
  wire T1;
  wire T2;
  wire T3;
  wire T4;
  wire T5;
  wire T6;
  wire T7;
  wire T8;

  assign io_out = T0;
  assign T0 = T7 ? 1'h0/* 0*/ : T1;
  assign T1 = T4 ? io_b : T2;
  assign T2 = T3 ? io_a : 1'h0/* 0*/;
  assign T3 = io_select == 1'h0/* 0*/;
  assign T4 = T6 && T5;
  assign T5 = io_select == 1'h1/* 1*/;
  assign T6 = ! T3;
  assign T7 = ! T8;
  assign T8 = T3 || T5;
endmodule

