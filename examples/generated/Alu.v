module Alu(
    input [9:0] io_sw,
    output[9:0] io_led
);

  wire[9:0] T0;
  wire[3:0] result;
  wire[3:0] T1;
  wire[3:0] T2;
  wire[3:0] T3;
  wire[3:0] T4;
  wire[3:0] T5;
  wire[3:0] T6;
  wire[3:0] b;
  wire[3:0] a;
  wire T7;
  wire[1:0] T8;
  wire[1:0] fn;
  wire[3:0] T9;
  wire T10;
  wire[1:0] T11;
  wire[3:0] T12;
  wire T13;
  wire[3:0] T14;
  wire T15;

  assign io_led = T0;
  assign T0 = {6'h0/* 0*/, result};
  assign result = T1;
  assign T1 = T15 ? T14 : T2;
  assign T2 = T13 ? T12 : T3;
  assign T3 = T10 ? T9 : T4;
  assign T4 = T7 ? T6 : T5;
  assign T5 = {3'h0/* 0*/, 1'h0/* 0*/};
  assign T6 = a + b;
  assign b = io_sw[4'h9/* 9*/:3'h6/* 6*/];
  assign a = io_sw[3'h5/* 5*/:2'h2/* 2*/];
  assign T7 = fn == T8;
  assign T8 = {1'h0/* 0*/, 1'h0/* 0*/};
  assign fn = io_sw[1'h1/* 1*/:1'h0/* 0*/];
  assign T9 = a - b;
  assign T10 = fn == T11;
  assign T11 = {1'h0/* 0*/, 1'h1/* 1*/};
  assign T12 = a | b;
  assign T13 = fn == 2'h2/* 2*/;
  assign T14 = a & b;
  assign T15 = fn == 2'h3/* 3*/;
endmodule

