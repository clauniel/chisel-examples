
import Chisel._
import Node._
import scala.collection.mutable.HashMap

class Counter extends Module {
	val io = new Bundle {
		val button = UInt( INPUT, 1 )
		val leds = UInt( OUTPUT, 8 )
	}
	val counterReg = Reg( init = UInt( 0, 8 ) )
	val delayReg = Reg( init = UInt( 0, 25 ) )
	val delayCycles = 12500000
	when ( reset ) {
		counterReg := UInt( 0 )
		delayReg := UInt( 0 )
	} .elsewhen( io.button === UInt( 0 ) ) {
		delayReg := delayReg + UInt( 1 ) 
	} .otherwise {
		counterReg := counterReg 
	}
	when ( delayReg === UInt( delayCycles ) ) {
		counterReg := counterReg + UInt( 1 )
		delayReg := UInt( 0 )
	}
	io.leds := counterReg
}


object CounterMain {
	def main( args: Array[String] ): Unit = {
		chiselMain( args, () => Module( new Counter()))
	}
}

class CounterTester( dut: Counter ) extends Tester( dut, Array( dut.io ) ) {
	defTests {
		var allGood = true
		val vars = new HashMap[ Node, Node ]()

		vars(dut.io.button) = UInt( 1 )
		val leds = UInt( 0, 8 )
		vars( dut.io.leds ) = leds
		allGood = step( vars ) & allGood;
		vars( dut.io.button ) = UInt( 0 )
		allGood = step( vars ) & allGood
		vars( dut.io.leds ) = leds + UInt( 1 )
		allGood = step( vars ) & allGood 
		vars( dut.io.leds ) = leds + UInt( 1 )
		allGood = step( vars ) & allGood
		allGood = step( vars ) & allGood
		allGood = step( vars ) & allGood
		allGood = step( vars ) & allGood
		allGood = step( vars ) & allGood
		allGood = step( vars ) & allGood
		allGood = step( vars ) & allGood
		allGood = step( vars ) & allGood
		allGood
	}
}

object CounterTester {
	def main( args: Array[String] ): Unit = {
		chiselMainTest( args, () => Module( new Counter ) ) {
			f => new CounterTester( f )
		}
	}
}


