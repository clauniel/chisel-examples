module Counter(input clk, input reset,
    input  io_button,
    output[7:0] io_leds
);

  reg[7:0] counterReg;
  wire T0;
  wire T1;
  wire[24:0] T2;
  reg[24:0] delayReg;
  wire T3;
  wire T4;
  wire T5;
  wire T6;
  wire T7;
  wire[24:0] T8;
  wire[24:0] T9;
  wire[24:0] T10;
  wire[24:0] T11;
  wire[24:0] T12;
  wire[24:0] T13;
  wire T14;
  wire T15;
  wire T16;
  wire[7:0] T17;
  wire[7:0] T18;
  wire[7:0] T19;
  wire[7:0] T20;
  wire[7:0] T21;

  assign io_leds = counterReg;
  assign T0 = T14 || T1;
  assign T1 = delayReg == T2;
  assign T2 = {1'h0/* 0*/, 24'hbebc20/* 12500000*/};
  assign T3 = T4 || T1;
  assign T4 = !reset || T5;
  assign T5 = T7 && T6;
  assign T6 = io_button == 1'h0/* 0*/;
  assign T7 = reset;
  assign T8 = T1 ? T13 : T9;
  assign T9 = T5 ? T11 : T10;
  assign T10 = {24'h0/* 0*/, 1'h0/* 0*/};
  assign T11 = delayReg + T12;
  assign T12 = {24'h0/* 0*/, 1'h1/* 1*/};
  assign T13 = {24'h0/* 0*/, 1'h0/* 0*/};
  assign T14 = !reset || T15;
  assign T15 = ! T16;
  assign T16 = !reset || T6;
  assign T17 = T1 ? T20 : T18;
  assign T18 = T15 ? counterReg : T19;
  assign T19 = {7'h0/* 0*/, 1'h0/* 0*/};
  assign T20 = counterReg + T21;
  assign T21 = {7'h0/* 0*/, 1'h1/* 1*/};

  always @(posedge clk) begin
    if(!reset) begin
      counterReg <= 8'h0/* 0*/;
    end else if(T0) begin
      counterReg <= T17;
    end
    if(!reset) begin
      delayReg <= 25'h0/* 0*/;
    end else if(T3) begin
      delayReg <= T8;
    end
  end
endmodule

