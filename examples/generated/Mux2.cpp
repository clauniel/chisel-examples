#include "Mux2.h"

void Mux2_t::init ( bool rand_init ) {
}
void Mux2_t::clock_lo ( dat_t<1> reset ) {
  val_t T0__w0;
  T0__w0 = Mux2__io_select.values[0] == 0x0L;
  val_t T1__w0;
  { val_t __mask = -T0__w0; T1__w0 = 0x0L ^ ((0x0L ^ Mux2__io_a.values[0]) & __mask); }
  val_t T2__w0;
  T2__w0 = Mux2__io_select.values[0] == 0x1L;
  val_t T3__w0;
  T3__w0 = !T0__w0;
  val_t T4__w0;
  { T4__w0 = T3__w0&&T2__w0; }
  val_t T5__w0;
  { val_t __mask = -T4__w0; T5__w0 = T1__w0 ^ ((T1__w0 ^ Mux2__io_b.values[0]) & __mask); }
  val_t T6__w0;
  { T6__w0 = T0__w0||T2__w0; }
  val_t T7__w0;
  T7__w0 = !T6__w0;
  val_t T8__w0;
  { val_t __mask = -T7__w0; T8__w0 = T5__w0 ^ ((T5__w0 ^ 0x0L) & __mask); }
  { Mux2__io_out.values[0] = T8__w0; }
}
void Mux2_t::clock_hi ( dat_t<1> reset ) {
}
int Mux2_t::clock ( dat_t<1> reset ) {
  uint32_t min = ((uint32_t)1<<31)-1;
  if (clk_cnt < min) min = clk_cnt;
  clk_cnt-=min;
  if (clk_cnt == 0) clock_lo( reset );
  if (clk_cnt == 0) clock_hi( reset );
  if (clk_cnt == 0) clk_cnt = clk;
  return min;
}
void Mux2_t::print ( FILE* f ) {
  fprintf(f, "%s", TO_CSTR(Mux2__io_out));
  fprintf(f, "\n");
  fflush(f);
}
bool Mux2_t::scan ( FILE* f ) {
  str_to_dat(read_tok(f), Mux2__io_select);
  str_to_dat(read_tok(f), Mux2__io_a);
  str_to_dat(read_tok(f), Mux2__io_b);
  return(!feof(f));
}
void Mux2_t::dump(FILE *f, int t) {
}
